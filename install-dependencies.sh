set -e
set -x

PLATFORM=`uname`

install() {
  brew install "$1" || pacaur -S "$1" || apt-get install "$1" || exit 1
}

install shfmt
install gettext
