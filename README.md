# Nowaker's Visual Studio Code settings and keybindings

## Usage

1. Rename your existing files.
   - `mv settings.json settings.json.bak`
   - `mv keybindings.json keybindings.json.bak`
2. Clone into your config directory.
   - Linux: `git clone git@gitlab.com:Nowaker/dotfiles-vscode.git ~/.config/Code/User`
   - OSX: `git clone git@gitlab.com:Nowaker/dotfiles-vscode.git ~/Library/Application Support/Code/User`

## Favorite extensions

- Install extensions: `./install-extensions.sh`
  - Install system dependencies for extensions: `./install-dependencies.sh`
- Export your extensions: `./export-extensions.sh`

## Favorite settings

Favorite settings influenced by KDE, Ruby and IntelliJ IDEA.

## IntelliJ IDEA-like keybindings for Linux and remapped OSX

Usage: `./generate-keybindings.sh`

With this layout, there's virtually no differences between Linux and OSX keyboard layout (stroke-perfect),
and no significant differences in other applications (most Modifiers + Arrows combinations are slightly different).
See [keybindings.template.jsonc](keybindings.template.jsonc) for more details.

Several non-IntelliJ IDEA shortcuts are added:

- ALT+C - close panel
- CTRL+SHIFT+K - clear terminal scrollback
- CTRL+ALT+SHIFT+A - toggle git blame annotations

Terminal shortcuts:

- CTRL+ALT+LEFT/RIGHT - navigate across terminals
- CTRL+T - create new terminal
