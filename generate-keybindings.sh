set -e

CTRL=ctrl envsubst '$CTRL' < keybindings.template.jsonc > keybindings-linux.jsonc
CTRL=cmd envsubst '$CTRL' < keybindings.template.jsonc > keybindings-darwin.jsonc

PLATFORM=$(uname | tr '[:upper:]' '[:lower:]')

set -x
ln -s keybindings-$PLATFORM.jsonc keybindings.json $@